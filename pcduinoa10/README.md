# Mainline Kernel on pcDuino A10
*This was last updated for linux kernel 5.18.*

## Basic sunxi-defconfig
Don't need to do this if we already have an existing, customised `.config`.

```shell
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- sunxi_defconfig
```

## Modification of .config using menuconfig
```shell
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- menuconfig
```

## Building the zImage
```shell
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make -j9 zImage
```

Copy `arch/arm/boot/zImage` to `boot` partition.

## Builing the dtb
```shell
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make -j9 dtbs
```

Copy compiled device tree `arch/arm/boot/dts/sun4i-a10-pcduino.dtb` to `boot` partition.

## Kernel modules
```shell
mkdir /tmp/pcduino-kmod
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- make -j9 modules
ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=/tmp/pcduino-kmod make modules modules_install
```

Copy kernel modules to rootfs. e.g.
```
sudo cp -r /tmp/pcduino_kmod/lib/modules/5.4.0/ /media/ricky/89c092b3-e450-4947-8505-7d4d884e8f05/lib/modules/
```

## uboot boot.cmd
Create `boot.cmd` with the following contents.

```
setenv bootargs 'console=ttyS0,115200 root=/dev/mmcblk0p2 rw rootwait panic=10'
load mmc 0:1 0x43000000 sun4i-a10-pcduino.dtb || load mmc 0:1 0x43000000 boot/sun4i-a10-pcduino.dtb
load mmc 0:1 0x42000000 zImage || load mmc 0:1 0x42000000 boot/zImage
bootz 0x42000000 - 0x43000000
```

## uboot boot.scr
boot.cmd isn't used directly, but needs to be wrapped with uboot header with the command:
```
mkimage -C none -A arm -T script -d boot.cmd boot.scr
```

Copy `boot.cmd` and `boot.scr` to the `boot` partition.

## Kernel source headers (for building external kernel modules)
1. After successfully building the kernel using the instructions above, copy the source tree to
`/usr/src/`, e.g. `/usr/src/linux-x.y.z`. As the size can be quite big, the recommended way of
doing this is to eject the microsd card on the pcDuino and connect it to a PC, then do the
transfer there.
2. Assuming the transfer is done, insert the microsd card back to the pcDuino and power it up.
3. `sudo ln -sf /usr/src/linux-x.y.z /lib/modules/x.y.z/build`
4. `cd /usr/src/linux-x.y.z`
5. `sudo make modules_prepare`
6. `sudo make scripts`
7. To make dkms builds happy, in `/usr/src/linux-x.y.z/arch`, do `sudo ln -s arm armv7l`.

After performing the aforementioned steps, external kernel modules can now be compiled without issues.

## Getting userspace SPI dev to work
Use the modified `sun4i-a10-pcduino.dts` in this folder. It has the necessary bindings for spi0.

Then, enable the following kernel config items:
```
CONFIG_SPI_SUN4I=y
CONFIG_SPI_SUN6I=y
CONFIG_SPI=y
CONFIG_SPI_MASTER=y
CONFIG_EXPERIMENTAL=y
CONFIG_SPI_SPIDEV=y
```

