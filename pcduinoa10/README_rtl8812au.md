# Getting RTL8812au-based Wi-Fi dongles to work

## Overview
There is no mainline kernel driver for RTL8812au-based Wi-Fi dongles. However, the out-of-tree
driver can be found here: `https://github.com/aircrack-ng/rtl8812au/`. This is maintained by the
aircrack-ng community and has been confirmed to work with the pcDuino running Linux kernel v5.7.0
and the Edimax 7822UAC dongle.

## Prerequisites
- `/lib/modules/x.y.z` is set-up correctly (see README.md in the same folder).

## Instructions
See README.md from `https://github.com/aircrack-ng/rtl8812au/`.
